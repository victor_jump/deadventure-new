﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScene_End : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	 
	// Update is called once per frame
	void Update () {
		
	}

    void TheEnd()
    {
        SceneManager.LoadScene("Death");
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            CameraController.inst.WhiteIn();
            coll.gameObject.GetComponent<PlayerController>().gravity = false;
            Invoke("TheEnd", 3);
        }
    }
}
