﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScene_startController : MonoBehaviour {

    public GameObject firstPlatform;
    public GameObject secondPlatform;
    bool started = false;

	// Use this for initialization
	void Start () {
		
	}
	
    void FixedUpdate()
    {
 
        if (started)
        {
            if (firstPlatform.transform.rotation.eulerAngles.z >= 270 || firstPlatform.transform.rotation.eulerAngles.z == 0)
            {
                firstPlatform.transform.Rotate(new Vector3(0, 0, -1));
            }
            if (secondPlatform.transform.rotation.eulerAngles.z >= 270 || secondPlatform.transform.rotation.eulerAngles.z == 0)
            {
                secondPlatform.transform.Rotate(new Vector3(0, 0, -1));
            }
             
        }
    }

	// Update is called once per frame
	void Update () {
		
	}

    void BecomeActive()
    {
        GameManager.player.gravity = true;
        GameManager.player.floatComponent.SetMovement(false);
        GameManager.player.controlsActive = false;
        CameraController.ChangeState(CameraController.CameraState.LowView);
        started = true;
    }
}
