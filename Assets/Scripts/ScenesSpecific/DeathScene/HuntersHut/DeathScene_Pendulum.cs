﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScene_Pendulum : MonoBehaviour {
    public bool flesh = false;
    public AudioClip fleshSound;
    AudioSource pendulumSource;
	// Use this for initialization
	void Start () {
        pendulumSource = GetComponent<AudioSource>();
        Invoke("PlayPendulumSound", 1);
	}

    void PlayPendulumSound()
    { 
        pendulumSource.Play();
        Invoke("PlayPendulumSound", 1);
    }
    

	// Update is called once per frame
	void Update () {
		
	}
}
