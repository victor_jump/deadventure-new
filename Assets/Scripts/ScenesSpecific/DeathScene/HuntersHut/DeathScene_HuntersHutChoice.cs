﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class DeathScene_HuntersHutChoice : MonoBehaviour {

    public SpriteFadeAndAppear blackFade;
    public List<UITextAppearAndDissapear> textList;
    public UITextAppearAndDissapear tapText;
    public ParticleSystem FirstChoiceParticles;
    public ParticleSystem secondChoiceParticles;
    public InfoMessage UseMessage;
    int originalLayer;
    int originalPupilLayer;
    GameObject player;
    bool choiceActive = false;
    bool mousePartActive = false;
    bool leftArmChopped = true;
    bool choppedOff = false;
    public List<AudioClip> chopSounds;
    int chopCount = 0;
    int chopLimit = 40;
    public ParticleSystem bloodParticles;
    float soundLimit;
    float soundCounter = 0;
    public GameObject Door;
    public GameObject DoorObstacle;
    public GameObject Blood;
    public AudioClip doorShutClip;
    public SpriteRenderer clock;
    public GameObject deerCollider; 


    public void StartChoice()
    {
        foreach(UITextAppearAndDissapear text in textList)
        {
            text.Appear();
        }
        blackFade.Appear();
        FirstChoiceParticles.Play();
        secondChoiceParticles.Play();
        player.GetComponent<PlayerController>().body.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 123;
        player.GetComponent<PlayerController>().head.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 123;
        player.GetComponent<PlayerController>().leftPupil.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 124;
        player.GetComponent<PlayerController>().rightPupil.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 124;
        player.GetComponent<PlayerController>().controlsActive = false;
        choiceActive = true; 
        AudioManager.inst.musicSource.volume = 0f;
        GetComponent<AudioSource>().volume = 1;

    }


    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        originalLayer = player.GetComponent<PlayerController>().body.gameObject.GetComponent<SpriteRenderer>().sortingOrder;
        originalPupilLayer = player.GetComponent<PlayerController>().leftPupil.gameObject.GetComponent<SpriteRenderer>().sortingOrder;
        soundLimit = Random.Range(3, 5);
        //mousePartActive = true;
        //choiceActive = true;
	}



    void SecondChoiceStage()
    {

        FirstChoiceParticles.Stop();
        secondChoiceParticles.Stop();
        foreach (UITextAppearAndDissapear text in textList)
        {
            text.Fade();
        }
        tapText.Appear();
        mousePartActive = true;
    }
   
    void ProcessChoiceFirstPart()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            SecondChoiceStage();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {  
            leftArmChopped = false;
            SecondChoiceStage();
        }
    }
    

    void ThirdChoiceState()
    {
        Camera.main.gameObject.GetComponent<CameraShake>().ShakeCamera(1, 5);
        blackFade.Fade();
        tapText.Fade();
        if (leftArmChopped)
        {
            player.GetComponent<PlayerController>().ChangeBodySprite(1);
        }
        else
        {
            player.GetComponent<PlayerController>().ChangeBodySprite(2);
        }
        player.GetComponent<PlayerController>().controlsActive = true;
        Blood.SetActive(true);
        Door.SetActive(false);
        DoorObstacle.SetActive(false);
        player.GetComponent<PlayerController>().body.gameObject.GetComponent<SpriteRenderer>().sortingOrder = originalLayer;
        player.GetComponent<PlayerController>().head.gameObject.GetComponent<SpriteRenderer>().sortingOrder = originalLayer;
        player.GetComponent<PlayerController>().leftPupil.gameObject.GetComponent<SpriteRenderer>().sortingOrder = originalPupilLayer;
        player.GetComponent<PlayerController>().rightPupil.gameObject.GetComponent<SpriteRenderer>().sortingOrder = originalPupilLayer;
        AudioManager.inst.soundSource.PlayOneShot(doorShutClip);
        UseMessage.Suicide();
        clock.enabled = true;
        clock.gameObject.GetComponent<DeathScene_Pendulum>().flesh = true;
        deerCollider.SetActive(true); 
        GetComponent<AudioSource>().volume = 0;
    }

    void ProcessChoiceSecondPart()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            bloodParticles.Emit(Random.Range(20 + chopCount * 20, 50 + chopCount * 20));
            ParticleSystem.MainModule main = bloodParticles.main;
            soundCounter += 1;
            chopCount += 1;
            if (soundCounter >= soundLimit)
            {
                soundLimit = Random.Range(3, 5);
                soundCounter = 0;
                AudioManager.inst.soundSource.PlayOneShot(chopSounds[Random.Range(0, (chopSounds.Count - 1))]);
                 
            }
            if (chopCount >= chopLimit)
            {
                ThirdChoiceState();
            }
        }
    }

	
	// Update is called once per frame
	void Update () {
		 if(choiceActive)
        {
            if (!mousePartActive)
            {
                ProcessChoiceFirstPart();
            }
            else
            {
                if (!choppedOff)
                {
                    ProcessChoiceSecondPart();
                }
                else
                {

                }
            }
        }
	}
}
