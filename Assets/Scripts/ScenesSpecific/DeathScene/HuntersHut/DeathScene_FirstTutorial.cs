﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathScene_FirstTutorial : MonoBehaviour {

    public Text text;
    bool hasExecuted = false;

    IEnumerator FadeOutText()
    {
        for (float f = 1; f >= 0; f -= 0.01f)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, f);
            yield return null;
        }
        hasExecuted = true;
        //Debug.Log("Finished");
    }
    // Use this for initialization
    void Start () {
		
	}



    public void OnDisablingText()
    {
        if(!hasExecuted)
        StartCoroutine("FadeOutText");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
