﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScene_FirstAction : MonoBehaviour {

    bool investigatedClock = false;
    bool investigatedWindow = false;
    bool investigatedClockAgain = false;
    bool investigatedFish = false;
    bool investigatedDoor = false;
    bool investigatedClockThird = false;
    bool invesigatedFishAgain = false;
    public List<InfoMessage> firstActDisable;
    public List<GameObject> secondActEnable;
    public List<InfoMessage> secondActDisable;
    public List<GameObject> thirdActEnable;
    public List<InfoMessage> thirdActDisable;
    public List<GameObject> fourActEnable; 
    public DeathScene_HuntersHutChoice choice;

    public void OnClockMessage()
    {

 
        investigatedClock = true; 
    }

    public void ThirdAct()
    {

        if (investigatedClockThird && invesigatedFishAgain && investigatedDoor)
        {
            foreach (InfoMessage message in thirdActDisable)
            {
                message.Suicide();
            }
            foreach (GameObject message in fourActEnable)
            {
                message.SetActive(true);
            }
        }
    }

    public void OnClockMessageThree()
    {
        investigatedClockThird = true;
        ThirdAct();
    }

    public void OnFishMessageTwo()
    {
        invesigatedFishAgain = true;
        ThirdAct();
    }

    public void OnSecondDoorMessage()
    {
        investigatedDoor = true;
        ThirdAct();
    }

    public void OnClockMessageTwo()
    {
        investigatedClockAgain = true;
        if (investigatedClockAgain && investigatedFish)
        {
            SecondAct();
        }
    }

    public void OnHotFishMessage()
    {
        investigatedFish = true;
        if (investigatedClockAgain && investigatedFish)
        {
            SecondAct();
        }
    }

    public void OnWindowMessage()
    { 
        investigatedWindow = true;
        FirstAct();
    }



    void SecondAct()
    {
        foreach (InfoMessage message in secondActDisable)
        {
            message.Suicide();
        }
        foreach (GameObject message in thirdActEnable)
        {
            message.SetActive(true);
        }
    }


    void FirstAct()
    {
        if (investigatedWindow && investigatedClock)
        { 
            foreach(InfoMessage message in firstActDisable)
            {
                message.Suicide();
            } 
            foreach(GameObject message in secondActEnable)
            {
                message.SetActive(true);
            }
        }
    }

	// Use this for initialization
	void Start () {

        //investigatedClock = true;
        //investigatedWindow = true;
        //investigatedClockAgain = true;
        //investigatedFish = true;
        //investigatedDoor = true;
        //investigatedClockThird = true;
        //invesigatedFishAgain = true;
        //FirstAct();
        //SecondAct();
        //ThirdAct();
    }


    public void OnFourAct()
    {
        choice.StartChoice();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
