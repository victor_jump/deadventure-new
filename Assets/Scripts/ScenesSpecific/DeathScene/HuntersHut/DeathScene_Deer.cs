﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScene_Deer : MonoBehaviour {

 
    bool started = false;
    public Transform DeerHead;
    public GameObject DeerHead2;
    public GameObject DeerMouth; 
    public UITextAppearAndDissapear text;
    public float angle;
    public AudioClip creak; 
	// Use this for initialization
	void Start () {
 
	}

    IEnumerator Turn()
    {
        for(float f = 0; f <= 20; f += 1f)
        {
            DeerHead.rotation = Quaternion.Euler(DeerHead.rotation.eulerAngles.x, DeerHead.rotation.eulerAngles.y, f);
            yield return null;
        }
        yield return new WaitForSeconds(1);
        DeerHead.gameObject.SetActive(false);
        DeerMouth.SetActive(true);
        DeerHead2.SetActive(true);
        text.Appear();
        Camera.main.gameObject.GetComponent<CameraShake>().ShakeCamera(0.3f, 1); 
    } 

	public void StartAction()
    {
        if (!started)
        {
            StartCoroutine("Turn");
            AudioManager.inst.soundSource.PlayOneShot(creak);
            started = true;
        }
    }


    void ProcessPlayer()
    { 
    }

	// Update is called once per frame
	void Update () {
		
	}
     
}
