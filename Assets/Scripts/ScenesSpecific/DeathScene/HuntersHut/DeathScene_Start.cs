﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScene_Start : MonoBehaviour {
    public AudioClip glassBreaking;
	// Use this for initialization
	void Start () {
        AudioManager.inst.musicSource.volume = 0;
        CameraController.inst.white.color = new Color(CameraController.inst.white.color.r, CameraController.inst.white.color.g, CameraController.inst.white.color.b, 1);
        CameraController.inst.WhiteOut();
        AudioManager.inst.soundSource.PlayOneShot(glassBreaking, 0.2f);
        Invoke("StartMusic", 1.5f);
	}

    void StartMusic()
    {

        AudioManager.inst.musicSource.volume = 0.2f;
        AudioManager.inst.musicSource.pitch = 0.6f;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
