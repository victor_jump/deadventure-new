﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeHuntersHut : MonoBehaviour {

    public SpriteFadeAndAppear sprite;
    public GameObject HuntersHut;
    public AudioClip shutdown;

	// Use this for initialization
	void Start () {
		
	}
	

    public void StartAction()
    {
        sprite.Appear();
        AudioManager.inst.soundSource.PlayOneShot(shutdown, 3);

        Invoke("killHut", 2f);
    }

    void killHut()
    {
        HuntersHut.SetActive(false); 
        AudioManager.inst.musicSource.volume = 0.6f;
    }

	// Update is called once per frame
	void Update () {
		
	}
}
