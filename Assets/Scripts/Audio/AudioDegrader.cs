﻿using UnityEngine;
using System.Collections;

public class AudioDegrader : MonoBehaviour {

    float startPitch;
    float startVolume;
    float degradeTime, degradeTimeout; 
    AudioSource Audio;
	// Use this for initialization
	void Start () {
        Audio = GameObject.FindGameObjectWithTag("MainAudio").GetComponent<AudioSource>();
        startPitch = Audio.pitch;
        startVolume = Audio.volume;
        degradeTimeout = 0.2f;
        
	}
	
	// Update is called once per frame
	void Update () {
        
	}


    void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {


            degradeTime += Time.deltaTime;
            if (degradeTime >= degradeTimeout && Audio.pitch >= 0.3f)
            {
                Audio.pitch -= 0.01f; 
                degradeTime = 0;
            }
        }
        
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        Audio.volume = startVolume;
        Audio.pitch = startPitch;
    }
}
