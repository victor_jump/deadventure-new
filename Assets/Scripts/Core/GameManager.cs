﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static PlayerController player;
    public float cursorTimeout = 0.5f; 
    public Texture2D defaultCursor;
    public Texture2D openGLowingCursor;
    public Texture2D closedGLowingCursor;
    UnityEngine.Coroutine currentCoroutine;
    CursorState currentState;
    bool openHand = true;
    public static GameManager inst;

    IEnumerator GlowAndChange()
    {
        while (true)
        {
            if (openHand)
            {
                Cursor.SetCursor(openGLowingCursor, new Vector2(0, 0), CursorMode.Auto);
                openHand = false;
            }
            else
            {
                Cursor.SetCursor(closedGLowingCursor, new Vector2(0, 0), CursorMode.Auto);
                openHand = true;
            }
            yield return new WaitForSeconds(0.5f);
        } 
    }

    public void ChangeCursor(CursorState iState)
    {
        switch(iState)
        {
            case CursorState.Standart:
                if (currentCoroutine != null)
                {
                    StopCoroutine(currentCoroutine);
                    Cursor.SetCursor(defaultCursor, new Vector2(0, 0), CursorMode.Auto);

                }
                break;
            case CursorState.Glowing:
                currentCoroutine = StartCoroutine("GlowAndChange");
                break;
            default:
                break;
        }
    }

    

    // Use this for initialization
    void Start () {
        inst = this;
        currentState = CursorState.Standart;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        Cursor.SetCursor(defaultCursor, new Vector2(0, 0), CursorMode.Auto); 
	}
	
	// Update is called once per frame
	void Update () {
		
	}




    public enum CursorState
    {
        Standart,
        Glowing
    }
}
