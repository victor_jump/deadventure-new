﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour {

    public static Dictionary<CameraState, CameraStateData> CameraStateMap;
    public static CameraState currentState;
    public static CameraState targetState;
    public static float time = 1;
    public bool changingState = false;
    public static CameraController inst;
    public Image white; 
    Transform currentCamera;
    public static bool processState = false;


    IEnumerator WhiteInCoroutine()
    {
        for (float f = 0f; f <= 1; f += 0.01f)
        {
            white.color = new Color(white.color.r, white.color.g, white.color.b, f); 
            yield return null;
        }
    }

    IEnumerator WhiteOutCoroutine()
    {
        for (float f = 1f; f >= 0; f -= 0.01f)
        {
            white.color = new Color(white.color.r, white.color.g, white.color.b, f);
            yield return null;
        }
    }

    // Use this for initialization
    void Start () {
        CameraStateMap = new Dictionary<CameraState, CameraStateData>();
        CreateMap();
        currentState = CameraState.HighView;
        targetState = CameraState.HighView;
        currentCamera = Camera.main.transform;
        inst = this;
	}

    public void WhiteOut()
    {
        StartCoroutine("WhiteOutCoroutine");
    }

    public void WhiteIn()
    {
        StartCoroutine("WhiteInCoroutine");
    }

    public static void ChangeState(CameraState newState)
    {
        time = 0;
        targetState = newState;
        processState = true;
    }

    void ProcessState()
    { 
       time = Mathf.Clamp(time + 0.002f, 0, 1);
       if(time <= 1)
        {
            currentCamera.transform.rotation = Quaternion.Euler(Vector3.Lerp(CameraStateMap[currentState].Rotation.eulerAngles, CameraStateMap[targetState].Rotation.eulerAngles, time)); 
            if(time == 1)
            {
                currentState = targetState;
                processState = false;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        if (processState)
        {
            ProcessState();
        }
    }
    
    void CreateMap()
    {
        CameraStateMap.Add(CameraState.HighView, new CameraStateData(new Vector3(-0.34f, 6.14f, -20), Quaternion.Euler(0, 0, 0)));
        CameraStateMap.Add(CameraState.ShortView, new CameraStateData(new Vector3(-0.02f, 0.01f, -20), Quaternion.Euler(0, 0, 0)));
        CameraStateMap.Add(CameraState.LowView, new CameraStateData(new Vector3(-0.34f, 6.14f, -20), Quaternion.Euler(20, 5, 0)));


    }

    public struct CameraStateData
    {
        public CameraStateData(Vector3 iPosition, Quaternion iRotation)
        {
            this.Position = iPosition;
            this.Rotation = iRotation;
        }
        public Vector3 Position;
        public Quaternion Rotation;
    }

    public enum CameraState
    {
        HighView, 
        ShortView,
        LowView,
    }
}
