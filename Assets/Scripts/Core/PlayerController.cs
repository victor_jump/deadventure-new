﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //MovementRelated
    float MovementStep = 5f; 
    public bool controlsActive = true;
    public bool gravity = false;
    public Floating floatComponent;
    public Transform body;
    public Transform head;
    public List<Sprite> bodySprites;
    SpriteRenderer bodySprite; 
    //Eye related
    public Transform leftPupil;
    public Transform rightPupil;     
    Vector3 leftPupilStart;
    Vector3 rightPupilStart;
    float eyeRadius = 0.1f;
    public float distanceRadius = 2;
    public Sprite_Direction currentSpriteDirection;
    //obstacleRelated
    bool leftObstacle = false;
    bool rightObstacle = false;
    
	// Use this for initialization
	void Start () {
        bodySprite = body.GetComponent<SpriteRenderer>();
	}    

    void ProcessGravity()
    {
        transform.position += new Vector3(0, -0.05f, 0);
    }


    public void ChangeBodySprite(int i)
    {
        bodySprite.sprite = bodySprites[i];
    }

    void OnDrawGizmos()
    {
       Gizmos.color = Color.green;
       Gizmos.DrawWireSphere(transform.position, distanceRadius);
    }

    void ProcessEyes()
    { 
        Collider2D[] listOfObjects = Physics2D.OverlapCircleAll(transform.position, distanceRadius, 1 << LayerMask.NameToLayer("PointOfInterest"), 0); 
        if (listOfObjects.Length > 0)
        { 
            Transform target = listOfObjects[0].transform;
            foreach (Collider2D coll in listOfObjects)
            {
                if((target.transform.position - transform.position).magnitude > (coll.transform.position - transform.position).magnitude)
                {
                    target = coll.transform;
                }
            }
            Vector3 direction = target.transform.position - transform.position;
            
            Vector3 leftPupilVector = Vector3.Lerp(leftPupil.parent.position, leftPupil.parent.position + direction.normalized * eyeRadius, ((distanceRadius - 3) - direction.magnitude) / (distanceRadius - 3));
            Vector3 rightPupilVector = Vector3.Lerp(rightPupil.parent.position, rightPupil.parent.position + direction.normalized * eyeRadius, ((distanceRadius - 3) - direction.magnitude) / (distanceRadius - 3));
            Vector3 velocity = direction.normalized;
            leftPupil.position = leftPupilVector;
            rightPupil.position = rightPupilVector;

        }
    }

    void ProcessDirection()
    {
        if(currentSpriteDirection == Sprite_Direction.Left)
        {
            if(body.rotation.eulerAngles.y < 180)
            {
                body.rotation = Quaternion.Euler(body.rotation.eulerAngles.x, Mathf.Clamp(body.rotation.eulerAngles.y + 5, 0, 180), body.rotation.eulerAngles.z);
            }
        }
        if(currentSpriteDirection == Sprite_Direction.Right)
        {
            if (body.rotation.eulerAngles.y > 0)
            {
                body.rotation = Quaternion.Euler(body.rotation.eulerAngles.x, Mathf.Clamp(body.rotation.eulerAngles.y - 5, 0, 180), body.rotation.eulerAngles.z);
            }
        }
    }

    void ProcessControls()
    { 
        {
            if (Input.GetKey(KeyCode.A))
            {
                if (!leftObstacle)
                { 
                    transform.Translate(new Vector3(-1, 0, 0) * MovementStep * Time.deltaTime);
                    currentSpriteDirection = Sprite_Direction.Left;
                }
            }
            if (Input.GetKey(KeyCode.D))
            {
                if (!rightObstacle)
                {
                    transform.Translate(new Vector3(1, 0, 0) * MovementStep * Time.deltaTime);
                    currentSpriteDirection = Sprite_Direction.Right;
                }
            }
        }
    }

    void ProcessObstacles(Collider2D coll)
    { 
        if (coll.gameObject.tag == "Obstacle")
        { 
            if ((coll.gameObject.transform.position - transform.position).x > 0)
            {
                rightObstacle = true;
               // transform.position -= new Vector3(0.1f, 0, 0);
            }
            else
            {
                leftObstacle = true;
                // transform.position += new Vector3(0.1f, 0, 0);
            }
        }
    }

    void FixedUpdate()
    {
        ProcessDirection();
        ProcessEyes();
         
    }
 

	// Update is called once per frame
	void Update () {

        if (controlsActive)
        {
            ProcessControls();
        }
        if (gravity)
        {
            ProcessGravity();
        } 
    }


    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Obstacle")
        {
            rightObstacle = false;
            leftObstacle = false;
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        ProcessObstacles(coll); 
    }

     
    public enum Sprite_Direction
    {
        Right,
        Left
    }
}
