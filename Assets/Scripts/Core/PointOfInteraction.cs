﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInteraction : MonoBehaviour {

    bool isInside = false;
    
	// Use this for initialization
	void Start () {
		
	}

    void OnMouseOver()
    { 
        if (!isInside)
        {
            GameManager.inst.ChangeCursor(GameManager.CursorState.Glowing);
        }
        isInside = true;
    }

    void OnMouseExit()
    { 
        if (isInside)
        {
            GameManager.inst.ChangeCursor(GameManager.CursorState.Standart);
        }
        isInside = false;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
