﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class InfoMessage : MonoBehaviour {

    public float activeTimeDuration = 10;
    public Text text;
    public ParticleSystem backgroundParticles;
    public ParticleSystem clueParticles;
    public Color readParticlesColor;
    public UnityEvent endCallback; 
    [Multiline]
    public string textLines; 
    bool inProcess = false;
    bool dying = false;
	// Use this for initialization
	void Start () {
        text.color = new Color(text.color.r, text.color.g, text.color.b, 0);
        text.text = textLines;
	}


    public void Suicide()
    {
        ParticleSystem.MainModule main = clueParticles.main; 
        main.loop = false; 
        dying = true;
        Invoke("Death", main.duration+3); 
    }

    void Death()
    {
        this.gameObject.SetActive(false);
    }

    IEnumerator FadeInText()
    { 
        ParticleSystem.MainModule module = clueParticles.main;
        module.startLifetime = activeTimeDuration + 2f;
        module.startColor = readParticlesColor;
        backgroundParticles.Play();
        for (float f = 0; f <= 1; f += 0.01f)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, f);
            yield return null;
        }
        yield return new WaitForSeconds(activeTimeDuration);
        StartCoroutine("FadeOutText");
        //Debug.Log("Finished");
    }

    IEnumerator FadeOutText()
    {
        for (float f = 1; f >= 0; f -= 0.01f)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, f);
            yield return null;
        }
        inProcess = false;
        if(endCallback != null)
        {
            endCallback.Invoke();
        }
         //Debug.Log("Finished");
    }

    // Update is called once per frame
    void Update () {
		
	}

    void BecomeActive()
    {
        if (!inProcess && !dying)
        {
            
            inProcess = true;
            StartCoroutine("FadeInText");
        }
    }
     
}
