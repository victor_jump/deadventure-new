﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UITextAppearAndDissapear : MonoBehaviour {


    bool inProgress = false;
    public Text text;
    public bool startTransparent = true;
    

    IEnumerator TextFade()
    {
        for (float f = 1; f >= 0; f -= 0.01f)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, f);
            yield return null;
        }
    }

    IEnumerator TextAppear()
    {
        for (float f = 0; f <= 1; f += 0.01f)
        {

            text.color = new Color(text.color.r, text.color.g, text.color.b, f);
            yield return null;
        }
    }


    public void Appear()
    {
        if (!inProgress)
        {
            StartCoroutine("TextAppear");
        }
        
    }

    public void Fade()
    {
        if (!inProgress)
        {
            StartCoroutine("TextFade");
        }
    }



	// Use this for initialization
	void Start () {
		if(startTransparent)
        {

            text.color = new Color(text.color.r, text.color.g, text.color.b, 0);
        }
        else
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
