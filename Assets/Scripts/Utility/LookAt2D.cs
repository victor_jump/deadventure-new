﻿using UnityEngine;
using System.Collections;


public class LookAt2D : MonoBehaviour {

        const float radian = 57;
        public GameObject target;
        void lookAt()
        {
            // rotating vector to face the cursor, for more information google it
            Vector3 pos = target.transform.position;
            float AngleRad = Mathf.Atan2(pos.y - transform.position.y, pos.x - transform.position.x);
            float AngleDeg = (180 / Mathf.PI) * AngleRad;
            this.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        }
        // Use this for initialization
        void Start()
        {


        }
        // Update is called once per frame
        void Update()
        {
            //cursor position
            lookAt();
        }
    
}
