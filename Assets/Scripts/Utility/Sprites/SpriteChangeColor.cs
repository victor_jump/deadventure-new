﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChangeColor : MonoBehaviour {

    SpriteRenderer currentRenderer;
    public Color endColor;
    Color startColor;

    IEnumerator ToTargetColor()
    {

        for (float f = 0; f <= 1; f += 0.01f)
        {
            currentRenderer.color = Color.Lerp(startColor, endColor, f);
        }
        yield return null;
    }

    IEnumerator ToStartColor()
    {

        for (float f = 0; f <= 1; f += 0.01f)
        {
            currentRenderer.color = Color.Lerp(endColor, startColor, f);
        }
        yield return null;
    }


    public void MoveToTargetColor()
    {
        StartCoroutine("ToTargetColor");
    }


    public void MoveToStartColor()
    {
        StartCoroutine("ToStartColor");
    }

    // Use this for initialization
    void Start () {
        currentRenderer = GetComponent<SpriteRenderer>();
        startColor = currentRenderer.color;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
