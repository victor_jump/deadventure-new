﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFadeAndAppear : MonoBehaviour {

    bool inProgress = false;
    public float targetAlpha = 1;
    public SpriteRenderer SpriteRenderer;
    public bool startTransparent = true; 

    IEnumerator SpriteFade()
    {
        for (float f = 1; f >= 0; f -= 0.01f)
        {
            SpriteRenderer.color = new Color(SpriteRenderer.color.r, SpriteRenderer.color.g, SpriteRenderer.color.b, f);
            yield return null;
        }
    }

    IEnumerator SpriteAppear()
    {
        for (float f = 0; f <= targetAlpha; f += 0.01f)
        {

            SpriteRenderer.color = new Color(SpriteRenderer.color.r, SpriteRenderer.color.g, SpriteRenderer.color.b, f);
            yield return null;
        }
    }


    public void Appear()
    {
        if (!inProgress)
        {
            StartCoroutine("SpriteAppear");
        }

    }

    public void Fade()
    {
        if (!inProgress)
        {
            StartCoroutine("SpriteFade");
        }
    }



    // Use this for initialization
    void Start()
    {
        if(startTransparent)
        {
            SpriteRenderer.color = new Color(SpriteRenderer.color.r, SpriteRenderer.color.g, SpriteRenderer.color.b, 0);
        }
        else
        {
            SpriteRenderer.color = new Color(SpriteRenderer.color.r, SpriteRenderer.color.g, SpriteRenderer.color.b, targetAlpha);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
