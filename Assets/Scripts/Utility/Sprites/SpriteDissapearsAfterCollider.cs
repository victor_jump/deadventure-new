﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteDissapearsAfterCollider : MonoBehaviour {

    public SpriteRenderer targetObject;
    bool started = false;

    IEnumerator FadeSprite()
    {
        for(float f = 1f; f > 0; f -= 0.01f)
        {
            Color c = targetObject.color;
            c.a = f;
            targetObject.color = c;
            yield return null; 
        }
    }

	// Use this for initialization
	void Start () { 
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            if (!started)
            {
                StartCoroutine("FadeSprite");
                started = true;
            }
        }
    }
}
