﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectActiveAfterKeyDown : MonoBehaviour {

    public GameObject targetObject;
    public KeyCode key;


	// Use this for initialization
	void Start () { 
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, targetObject.transform.position);
    }

    void OnTriggerStay2D(Collider2D coll)
    { 
        if(coll.gameObject.tag == "Player")
        {
            if(Input.GetKeyDown(key))
            { 
                targetObject.SendMessage("BecomeActive");
            }

        }
    }
}
