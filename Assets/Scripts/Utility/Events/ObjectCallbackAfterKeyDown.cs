﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ObjectCallbackAfterKeyDown : MonoBehaviour {
     
    public KeyCode key;
    public UnityEvent callback;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


     

    void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(key))
            {
                callback.Invoke();
            }

        }
    }
}
