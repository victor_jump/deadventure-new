﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ObjectCallbackAfterCollision : MonoBehaviour {

    public UnityEvent callback;
    bool active = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            if (!active)
            {
                active = true;
                callback.Invoke();
            }
        }
    }


}
