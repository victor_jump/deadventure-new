﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectActiveAfterTrigger : MonoBehaviour {

    public GameObject targetObject;
    bool active = false;

	// Use this for initialization
	void Start () { 
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, targetObject.transform.position);
    }

    void OnTriggerEnter2D(Collider2D coll)
    { 
        if(coll.gameObject.tag == "Player")
        {
            if (!active)
            {
                targetObject.SendMessage("BecomeActive");
                active = true;
            }
        }
    }
}
