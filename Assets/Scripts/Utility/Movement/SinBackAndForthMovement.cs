﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinBackAndForthMovement : MonoBehaviour {

    public float speed = 2f;
    public float maxRotation = 45f;
    void Update()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, maxRotation * Mathf.Sin(Time.time * speed), transform.rotation.eulerAngles.y);
    }
}
