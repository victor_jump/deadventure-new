﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour {
    [SerializeField]
    float Amplitude;
    [SerializeField]
    float speed; 
    float StartPos;
    float time = 0;
    bool up = true;
    bool movement = false;
    public bool syncro = false;

	// Use this for initialization
	void Start () {
        StartPos = transform.position.y + Amplitude;
        if (syncro)
        {
            movement = true;
        }
        else
        {
            Invoke("BeginMovement", Random.Range(0, 0.2f));
        }
	}

    public void SetMovement(bool iMovement)
    {
        movement = iMovement;
    }

    void BeginMovement()
    {
        movement = true;
    }

    void FixedUpdate()
    {
        if (movement)
        {
            if (up)
            {
                transform.position = new Vector3(transform.position.x, Mathf.Lerp(StartPos, StartPos + Amplitude, time), transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, Mathf.Lerp(StartPos + Amplitude, StartPos, time), transform.position.z);
            }
            if (time >= 1)
            {
                time = 0;
                up = !up;
            }
        }
        if (movement)
        {
            time += speed;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
       
    }
}
