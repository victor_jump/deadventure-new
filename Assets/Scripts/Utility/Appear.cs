﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Appear : MonoBehaviour {

    public Light appearingLight;
    public Text text;
    bool started = false;
    float startIntensity;
    public bool playSound = false;
    float time = 0;
    public AudioClip sound;

	// Use this for initialization
	void Start () {
        startIntensity = appearingLight.intensity;
        appearingLight.intensity = 0;
        text.color = new Color(text.color.r, text.color.g, text.color.b, 0);


    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (started)
        {
            if(time < 1)
            {
                appearingLight.intensity = Mathf.Lerp(0, startIntensity, time);
                text.color = new Color(text.color.r, text.color.g, text.color.b, Mathf.Lerp(0, 1, time));
                time += 0.005f; 
            }
        }
	}

    void BecomeActive()
    {
 
        started = true;
        if (playSound == true)
        {
            AudioManager.inst.soundSource.PlayOneShot(sound, 0.1f);
        }
    }
}
