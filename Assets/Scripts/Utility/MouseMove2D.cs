﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
 
public class MouseMove2D : MonoBehaviour
{

 
    public float moveSpeed = 0.1f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
 
        Debug.Log(Input.mousePosition.x + " " + Input.mousePosition.y);
        Vector3 newVector = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 20));
        transform.position = new Vector3(newVector.x, newVector.y, 0);
         
    }
} 
